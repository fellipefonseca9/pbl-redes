/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miservice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import model.Cliente;
import model.Consumo;
import model.Relatorio;
import model.Sensor;

/**
 *
 * @author fellipefonseca
 */
public class ClienteThread implements Runnable {

    private final Socket conn;
    private InputStream input;
    private PrintStream output;
    private final String FLOWFILE = "maximaVazao.txt";
    private final String CLIENTESFILE = "clientes.txt";
    private final String SENSORESFILE = "sensores.txt";

    public ClienteThread(Socket socket) {
        this.conn = socket;
    }

    @Override
    public void run() {
        try {
            input = conn.getInputStream();
            output = new PrintStream(conn.getOutputStream());
            try (Scanner s = new Scanner(conn.getInputStream())) {
                while (s.hasNextLine()) {
                    String line = s.nextLine();
                    String[] split = line.split("\\|");
                    System.out.println(line);
                    //ADICIONAR DADOS DO SENSOR
                    if (split[0].equals("s")) {
                        Sensor sensor = new Sensor(split[1], split[2], split[3], 0);
                        try {
                            FileInputStream fi = new FileInputStream(new File(SENSORESFILE));
                            ObjectInputStream oi = new ObjectInputStream(fi);
                            HashMap<String, Sensor> sensores;
                            sensores = (HashMap<String, Sensor>) oi.readObject();
                            Sensor get = sensores.get(sensor.getId());
                            if (get == null) {
                                sensores.put(sensor.getId(), sensor);
                            }
                            System.out.println(sensores);
                            FileOutputStream f = new FileOutputStream(new File(SENSORESFILE));
                            ObjectOutputStream o = new ObjectOutputStream(f);
                            o.writeObject(sensores);
                        } catch (Exception ex) {
                            HashMap<String, Sensor> hash = new HashMap<>();
                            hash.put(sensor.getId(), sensor);
                            FileOutputStream f = new FileOutputStream(new File(SENSORESFILE));
                            ObjectOutputStream o = new ObjectOutputStream(f);
                            o.writeObject(hash);
                        }
                    } else if (split[0].equals("c")) {
                        String id = split[2];
                        if (split[1].equals("r")) {
                            readFlow(id);
                        } else if (split[1].equals("rel")) {
                            this.getRelatorio(id, split[3], split[4]);
                        }
                        if (split[1].equals("c")) {
                            cadastrarUsuario(split[2], split[3], split[4], split[5]);
                        } else if (split[1].equals("w")) {
                            MIService.setMaxVazao(id, split[3]);
                        } else if (split[1].equals("g")) {
                            getCliente(id);
                        }
                    }
                }
            }
            conn.close();
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    private void getCliente(String id) {
        try {
            FileInputStream fi = new FileInputStream(new File(CLIENTESFILE));
            ObjectInputStream oi = new ObjectInputStream(fi);
            HashMap<String, Cliente> clientes;
            clientes = (HashMap<String, Cliente>) oi.readObject();
            Cliente cliente = clientes.get(id);
            ObjectOutputStream outputStream = new ObjectOutputStream(conn.getOutputStream());
            outputStream.writeObject(cliente);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ClienteThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(ClienteThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void cadastrarUsuario(String nome, String email, String senha, String id) {
        Cliente cliente = new Cliente(nome, email, senha, id, null);
        try {
            FileInputStream fi = new FileInputStream(new File(CLIENTESFILE));
            ObjectInputStream oi = new ObjectInputStream(fi);
            HashMap<String, Cliente> clientes;
            clientes = (HashMap<String, Cliente>) oi.readObject();
            clientes.put(cliente.getId(), cliente);
            System.out.println("Clientes: " + clientes);
            FileOutputStream f = new FileOutputStream(new File(CLIENTESFILE));
            ObjectOutputStream o = new ObjectOutputStream(f);
            o.writeObject(clientes);
        } catch (Exception ex) {
            FileOutputStream f = null;
            try {
                HashMap<String, Cliente> hash = new HashMap<>();
                hash.put(cliente.getId(), cliente);
                f = new FileOutputStream(new File(CLIENTESFILE));
                ObjectOutputStream o = new ObjectOutputStream(f);
                o.writeObject(hash);
            } catch (FileNotFoundException ex1) {
                Logger.getLogger(ClienteThread.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (IOException ex1) {
                Logger.getLogger(ClienteThread.class.getName()).log(Level.SEVERE, null, ex1);
            } finally {
                try {
                    f.close();
                } catch (IOException ex1) {
                    Logger.getLogger(ClienteThread.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
        }
    }

    private void writeFile(String id, String value, String file) throws IOException {
        FileWriter fw = new FileWriter(file, true);
        try (PrintWriter writer = new PrintWriter(fw)) {
            writer.println(id + "|" + value);
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    private void readFlow(String userId) {
        File file = new File("vazoes/" + userId);
        file.getParentFile().mkdirs();
        List<String> list = new ArrayList<>();
        try {
            FileInputStream fi = new FileInputStream(new File("vazoes/" + userId));
            ObjectInputStream oi = new ObjectInputStream(fi);
            try {
                list = (List<String>) oi.readObject();
            } catch (IOException | ClassNotFoundException ex) {
            }
        } catch (IOException ex) {
        }
        String vazao = list.get(list.size() - 1).split("\\|")[2];
        output.println(vazao);

        try {
            conn.close();
        } catch (IOException ex) {
            Logger.getLogger(ClienteThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void getRelatorio(String id, String inicio, String fim) {
        ObjectOutputStream outputStream = null;
        try {
            List<Consumo> listaConsumo = new ArrayList<>();
            List<Relatorio> relatorio = new ArrayList<>();
            File file = new File("vazoes/" + id);
            file.getParentFile().mkdirs();
            List<String> list = new ArrayList<>();
            try {
                FileInputStream fi = new FileInputStream(new File("vazoes/" + id));
                ObjectInputStream oi = new ObjectInputStream(fi);
                try {
                    list = (List<String>) oi.readObject();
                } catch (IOException | ClassNotFoundException ex) {
                }
            } catch (IOException ex) {
            }
            for (String line : list) {
                String[] split = line.split("\\|");
                Consumo c = new Consumo();
                c.setDate(this.transformDateObect(split[1]));
                c.setValor(Double.parseDouble(split[2]));
                listaConsumo.add(c);
            }

            String[] dateStr = inicio.split("/");
            Date date = new Date(dateStr[1] + "/" + dateStr[0] + "/" + dateStr[2]);
            date.setHours(0);
            date.setMinutes(0);
            System.out.println(date);
            String[] date2Str = fim.split("/");
            Date date2 = new Date(date2Str[1] + "/" + date2Str[0] + "/" + date2Str[2]);
            date2.setHours(23);
            date2.setMinutes(59);

            listaConsumo = listaConsumo.stream().filter(c -> c.getDate().after(date) && c.getDate().before(date2)).collect(Collectors.toList());
            System.out.println(listaConsumo);
            boolean setInicial = true;
            int horaInicial = 0;
            String dataInicial = "";
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/YYYY");
            double vazao = 0;
            int count = 0;
            for (Consumo c : listaConsumo) {
                count++;

                if (setInicial == true) {
                    horaInicial = c.getDate().getHours();
                    dataInicial = format.format(c.getDate());
                    vazao = c.getValor();
                    setInicial = false;
                } else if (c.getDate().getHours() != horaInicial) {
                    Relatorio r = new Relatorio();
                    String hora = c.getDate().getHours() > 9 ? "" + c.getDate().getHours() : "0" + c.getDate().getHours();
                    r.setDate(dataInicial + " " + hora + ":00 - " + hora + ":59");
                    r.setValor(String.format("%.2f", c.getValor() - vazao).replace(",", "."));
                    relatorio.add(r);
                    vazao = c.getValor();
                    horaInicial = c.getDate().getHours();
                    dataInicial = format.format(c.getDate());

                } else if (count == listaConsumo.size()) {
                    Relatorio r = new Relatorio();
                    String hora = c.getDate().getHours() > 9 ? "" + c.getDate().getHours() : "0" + c.getDate().getHours();
                    String minutes = c.getDate().getMinutes() > 9 ? "" + c.getDate().getMinutes() : "0" + c.getDate().getMinutes();
                    r.setDate(dataInicial + " " + hora + ":00 - " + hora + ":" + minutes);
                    r.setValor(String.format("%.2f", c.getValor() - vazao).replace(",", "."));
                    relatorio.add(r);
                }
            }
            outputStream = new ObjectOutputStream(conn.getOutputStream());
            outputStream.writeObject(relatorio);
        } catch (IOException ex) {
            Logger.getLogger(ClienteThread.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                outputStream.close();
            } catch (IOException ex) {
                Logger.getLogger(ClienteThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private String formataAno(String x) {
        String[] split = x.split(" ");
        return split[split.length - 1];
    }

    private Date transformDateObect(String x) {
        String date = x.split(" ")[0];
        date = date.split("/")[1] + "/" + date.split("/")[0] + "/" + date.split("/")[2];
        String time = x.split(" ")[1];
        Date d = new Date(date);
        d.setHours(Integer.parseInt(time.split(":")[0]));
        d.setMinutes(Integer.parseInt(time.split(":")[1]));
        return d;
    }
}
