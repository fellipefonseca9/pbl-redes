package miservice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import model.Cliente;
import model.Sensor;
import model.Consumo;
import model.Relatorio;

/**
 *
 * @author fellipefonseca
 */
public class MIService {

    static final int PORT = 8000;
    private static final String FLOWFILE = "maximaVazao.txt";
    private static final String CLIENTESFILE = "clientes.txt";
    private static final String SENSORESFILE = "sensores.txt";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
        //MIService s = new MIService();
        //s.getRelatorio("1");
        new Thread(tcp).start();
        new Thread(udp).start();

    }

    
    private static Runnable tcp = () -> {
        ServerSocket server = null;
        try {
            server = new ServerSocket(PORT);
            while (true) {
                Socket conn = server.accept();
                new Thread(new ClienteThread(conn)).start();
            }

        } catch (IOException e) {
            System.out.println(e);
        }
    };

    private static Runnable udp = () -> {
        try {
            try (DatagramSocket ds = new DatagramSocket(8000)) {
                while (true) {
                    byte[] msg = new byte[256];
                    DatagramPacket pkg = new DatagramPacket(msg, msg.length);
                    ds.receive(pkg);
                    String value = new String(pkg.getData()).trim();
                    System.out.println(value);
                    String id = value.split("\\|")[0];
                    File file = new File("vazoes/" + id);
                    file.getParentFile().mkdirs();
                    List<String> list = new ArrayList<>();
                    String date = value.split("\\|")[1];
                    String vazao = value.split("\\|")[2];
                    FileInputStream fi = new FileInputStream(new File(SENSORESFILE));
                    ObjectInputStream oi = new ObjectInputStream(fi);
                    HashMap<String, Sensor> sensores = new HashMap<>();
                    Sensor get = new Sensor();
                    double vazaoDb = Double.parseDouble(vazao);
                    try {
                        sensores = (HashMap<String, Sensor>) oi.readObject();
                        get = sensores.get(id);
                        FileOutputStream f = new FileOutputStream(new File(SENSORESFILE));
                        ObjectOutputStream o = new ObjectOutputStream(f);

                        fi = new FileInputStream(new File("vazoes/" + value.split("|")[0]));
                        oi = new ObjectInputStream(fi);

                        try {
                            list = (List<String>) oi.readObject();
                        } catch (Exception ex) {
                        }

                        if (list.size() > 0) {
                            String last = list.get(list.size() - 1);
                            double lastValue = Double.parseDouble(last.split("\\|")[2]);
                            double diference = 0;
                            String lastDate = last.split("\\|")[1];
                            if (vazaoDb > get.getLastValue()) {
                                diference = vazaoDb - get.getLastValue();
                            } else if (get.getLastValue() > vazaoDb) {
                                diference = vazaoDb;
                            }
                            get.setLastValue(vazaoDb);
                            sensores.put(id, get);
                            o.writeObject(sensores);
                            double novaVazao = lastValue + diference;
                            value = id + "|" + date + "|" + novaVazao;
                            System.out.println("Vazão: " + novaVazao);
                        }

                        list.add(value);
                        if (MIService.getClienteMaxVazao(id) != null && Double.parseDouble(value.split("\\|")[2]) >= MIService.getClienteMaxVazao(id)) {
                            MIService.setMaxVazao(id, null);
                            String email = MIService.getEmail(id);
                            System.out.println("Enviando email para: " + email);
                            new Thread(new SendMail(email)).start();
                        }
                    } catch (IOException | ClassNotFoundException | NumberFormatException ex) {

                    }
                    FileOutputStream f = new FileOutputStream(new File(SENSORESFILE));
                    ObjectOutputStream o = new ObjectOutputStream(f);
                    get.setLastValue(vazaoDb);
                    sensores.put(id, get);
                    o.writeObject(sensores);
                    f = new FileOutputStream(new File("vazoes/" + value.split("|")[0]));
                    o = new ObjectOutputStream(f);
                    o.writeObject(list);
                }
            }
        } catch (SocketException ex) {
            System.out.println(ex);
        } catch (IOException ex) {
            System.out.println(ex);
        }
    };

    public static String getEmail(String id) {
        FileInputStream fi = null;
        try {
            fi = new FileInputStream(new File(CLIENTESFILE));
            ObjectInputStream oi = new ObjectInputStream(fi);
            HashMap<String, Cliente> clientes;
            clientes = (HashMap<String, Cliente>) oi.readObject();
            Cliente cliente = clientes.get(id);
            return cliente.getEmail();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MIService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MIService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MIService.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fi.close();
            } catch (IOException ex) {
                Logger.getLogger(MIService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return "";
    }

    public static void setMaxVazao(String id, String maxVazao) {
        try {
            FileInputStream fi = new FileInputStream(new File(CLIENTESFILE));
            ObjectInputStream oi = new ObjectInputStream(fi);
            HashMap<String, Cliente> clientes;
            clientes = (HashMap<String, Cliente>) oi.readObject();
            Cliente cliente = clientes.get(id);
            cliente.setMaxVazao(maxVazao);
            clientes.put(cliente.getId(), cliente);
            System.out.println("Clientes: " + clientes);
            FileOutputStream f = new FileOutputStream(new File(CLIENTESFILE));
            ObjectOutputStream o = new ObjectOutputStream(f);
            o.writeObject(clientes);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ClienteThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ClienteThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ClienteThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static Double getClienteMaxVazao(String id) {
        try {
            FileInputStream fi = new FileInputStream(new File(CLIENTESFILE));
            ObjectInputStream oi = new ObjectInputStream(fi);
            HashMap<String, Cliente> clientes;
            clientes = (HashMap<String, Cliente>) oi.readObject();
            Cliente cliente = clientes.get(id);
            if (cliente.getMaxVazao() != null) {
                return Double.parseDouble(cliente.getMaxVazao());
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MIService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MIService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MIService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }
}
