/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miservice;

import java.io.UnsupportedEncodingException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author fellipefonseca
 */
public class SendMail implements Runnable {

    Properties props;
    Session session;
    String to;
    public SendMail(String to) {
        this.to = to;
        props = new Properties();
        /**
         * Parâmetros de conexão com servidor Gmail
         */
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("fellipefonseca99@gmail.com", "mi01020304");
            }
        });
    }

   
    @Override
    public void run() {

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("fellipefonseca99@gmail.com", "Sistema de controle de agua"));

            Address[] toUser = InternetAddress.parse(to);

            message.setRecipients(Message.RecipientType.TO, toUser);
            message.setSubject("Máxima vazão atingida");
            message.setText("Opa, já atingiu a vazão máxima!");
            Transport.send(message);

        } catch (MessagingException e) {
            System.out.println(e);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(SendMail.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

   
}
