/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package miclient;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Cliente;
import model.Relatorio;

/**
 *
 * @author fellipefonseca
 */
public class MIClient {

    private final InetAddress ADDRESS;
    private final int PORT;
    private List<String> vazaoList = new ArrayList<>();

    public MIClient() throws FileNotFoundException, UnknownHostException {
        Scanner s = new Scanner(new File("setup.txt"));
        ADDRESS = InetAddress.getByName(s.nextLine());
        PORT = Integer.parseInt(s.nextLine());
    }

    public String getListaVazoes(String id) {
        try (Socket client = new Socket(ADDRESS, PORT)) {
            PrintStream output = new PrintStream(client.getOutputStream());
            output.println("c|r" + "|" + id);
            Scanner input = new Scanner(client.getInputStream());
            String vazao = "0";
            while (input.hasNextLine()) {
                vazao = input.nextLine();
            }
            client.close();
            return vazao;
        } catch (IOException ex) {
            Logger.getLogger(MIClient.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    
    
    public Cliente getCliente(String id) {
        try (Socket client = new Socket(ADDRESS, PORT)) {
            PrintStream output = new PrintStream(client.getOutputStream());
            output.println("c|g" + "|" + id);
            ObjectInputStream inStream = new ObjectInputStream(client.getInputStream());
            Cliente cliente = (Cliente) inStream.readObject();
            client.close();
            return cliente;

        } catch (IOException ex) {
            System.out.println(ex);
            Logger.getLogger(MIClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            System.out.println(ex);

            Logger.getLogger(MIClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

     public List<Relatorio> getRelatorio(String id, String inicio, String fim) {
        try (Socket client = new Socket(ADDRESS, PORT)) {
            PrintStream output = new PrintStream(client.getOutputStream());
            output.println("c|rel|" + id + "|" + inicio + "|"+ fim);
            ObjectInputStream inStream = new ObjectInputStream(client.getInputStream());
            List<Relatorio> relatorio = (List<Relatorio>) inStream.readObject();
            client.close();
            return relatorio;

        } catch (IOException ex) {
            System.out.println(ex);
            Logger.getLogger(MIClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            System.out.println(ex);

            Logger.getLogger(MIClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public void cadastrarUsuario(String nome, String email, String id, String senha) {
        try (Socket client = new Socket(ADDRESS, PORT)) {
            PrintStream output = new PrintStream(client.getOutputStream());
            output.println("c|c|" + nome + "|" + email + "|" + senha + "|" + id);
            client.close();
        } catch (IOException ex) {
            Logger.getLogger(MIClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setVazao(String id, String vazao) {
        try (Socket client = new Socket(ADDRESS, PORT)) {
            PrintStream output = new PrintStream(client.getOutputStream());
            output.println("c|w|" + id + "|" + vazao);
            client.close();
        } catch (IOException ex) {
            Logger.getLogger(MIClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, UnknownHostException {
       
     }

}
