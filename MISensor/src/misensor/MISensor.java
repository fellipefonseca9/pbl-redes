/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misensor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fellipefonseca
 */
public class MISensor {

    private final int PORT;
    private final InetAddress ADDRESS;
    private int ID;
    private final String ZONE;
    private final int USERID;

    public MISensor() throws UnknownHostException, FileNotFoundException {
        Scanner s = new Scanner(new File("setup.txt"));
        ADDRESS = InetAddress.getByName(s.nextLine());
        PORT = Integer.parseInt(s.nextLine());
        ID = Integer.parseInt(s.nextLine());
        USERID = Integer.parseInt(s.nextLine());
        ZONE = s.nextLine();
    }

    public void setup() {
        try (Socket client = new Socket(ADDRESS, PORT)) {
            PrintStream output = new PrintStream(client.getOutputStream());
            output.println("s|" + getID() + "|"+ USERID + "|" + ZONE);
            client.close();

        } catch (IOException ex) {
            Logger.getLogger(MISensor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

 
    public void sendToServer(String value) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/YYYY HH:mm");
        String msg = getID() + "|" + dateFormat.format(new Date()) + "|" + value;
        System.out.println(msg);
        DatagramPacket pkg = new DatagramPacket(msg.getBytes(), msg.length(), ADDRESS, PORT);
        DatagramSocket ds;
        try {
            ds = new DatagramSocket();
            ds.send(pkg);
            //System.out.println("Mensagem enviada para: " + ADDRESS.getHostAddress() + "\n"
            //      + "Porta: " + PORT + "\n" + "Mensagem: " + msg);
            ds.close();

        } catch (SocketException ex) {
            Logger.getLogger(MISensor.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (IOException ex) {
            Logger.getLogger(MISensor.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * @return the ID
     */
    public int getID() {
        return ID;
    }

    /**
     * @param ID the ID to set
     */
    public void setID(int ID) {
        this.ID = ID;
    }

}
